module.exports = {
  publicPath: "https://nocdn.fsf.org.cn/s0/",
  css: {
    loaderOptions: {
      css: {
        sourceMap: process.env.NODE_ENV !== "production" ? true : false
      }
    }
  }
};
