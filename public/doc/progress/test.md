## 赴戍登程口占示家人

**赴戍登程口占示家人**是两首七言律诗，为道光二十二年（1842年）八月十一日，林则徐于西安城告别家人，启程去戍守边疆时创作。其中“苟利国家生死以，岂因祸福避趋之”是最出名的两句诗，被后人传诵不绝。收录于《云左山房诗钞》卷六。

### 创作背景

道光二十一年（1841年）六月二十八日，道光帝下旨将林则徐流放伊犁，林则徐于七月十四日从镇海启程[1]:671—672，次年五月中旬抵达西安，因病滞留了两个月[1]:710—711。八月十一日，天空放晴，林则徐告别家人，随长子林汝舟、次子林聪彝和三子林拱枢启程去戍守边疆，临别前口占了两首诗，出城西去[1]:714—715。

### 诗词大意

```
出门一笑莫心哀，浩荡襟怀到处开。
时实难以无过立，达官非自有生来。
风涛回首空三岛，尘壤从头数九垓。
休信儿童轻薄语，嗤他赵老送灯台。

力微任重久神疲，再竭衰庸定不支。
苟利国家生死以，岂因祸福避趋之？
谪居正是君恩厚，养拙刚于戍卒宜。
戏与山妻谈故事，试吟断送老头皮。
```

第一首诗首联强作欢颜，表现了林则徐旷达的胸怀；颔联表达自己的问心无愧；颈联讥讽英国无人，表达自己要游历各地；尾联鄙弃了“赵老送灯台，一去更不来”的浑话。

第二首诗的首联用自己身体衰老、精力不济，来排解家人的忧思；颔联化用郑国大夫子产的“苟利社稷，死生以之”，其中“国家”、“生死”、“祸福”、“避趋”为偏义词，指“国”、“死”、“祸”、“避”，表现林则徐不计个人得失的情怀；前四句诗为颈联作铺垫，表现了其忠君爱国的思想；尾联用典，表达其浩大气度及乐观主义精神。

### 后续

林则徐非常喜爱“苟利国家生死以，岂因祸福避趋之”这两句诗，常吟诵以自励。道光二十一年十二月十一日，林则徐于漳州旧疾复发，但仍以这两句诗勉励自己，催促继续赶路。道光二十九年十一月廿一（1850年1月3日），林则徐携夫人灵柩返闽时途经长沙，左宗棠前去舟中拜见，临别时，林则徐手书“苟利国家生死以，岂因祸福避趋之”一联赠予左宗棠，此后左宗棠常以此联激励自己。