import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Reg from "./views/Reg.vue";
import Coop from "./views/Coop.vue";
import About from "./views/About.vue";
import Progress from "./views/Progress.vue";
import ProgressPost from "./views/ProgressPost.vue";
import MainNavbar from "./layout/MainNavbar.vue";
import MainFooter from "./layout/MainFooter.vue";
import StickFooter from "./layout/StickFooter.vue";

Vue.use(Router);

import VueLayers from "vuelayers";
import "vuelayers/lib/style.css"; // needs css-loader

Vue.use(VueLayers);

export default new Router({
  routes: [
    {
      path: "/",
      name: "Home",
      components: { default: Home, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 200, dark: true },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/reg",
      name: "Register",
      components: { default: Reg, header: MainNavbar, footer: StickFooter },
      props: {
        header: { colorOnScroll: 200 }
      }
    },
    {
      path: "/coop",
      name: "Cooperation",
      components: { default: Coop, header: MainNavbar, footer: StickFooter },
      props: {
        header: { colorOnScroll: 100 }
      }
    },
    {
      path: "/about",
      name: "About",
      components: { default: About, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/progress",
      name: "Progress",
      components: { default: Progress, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/progress/:post",
      name: "ProgressPost",
      components: {
        default: ProgressPost,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: "black" }
      }
    }
  ],
  mode: "history",
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
