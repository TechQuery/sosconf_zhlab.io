import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    weibo_modal: false
  },
  mutations: {
    update_weibo_modal: (state, data) => {
      state.weibo_modal = data;
    }
  },
  actions: {},
  modules: {}
});
